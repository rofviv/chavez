
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/completed/:ci', name:"completed", component: () => import('pages/Completed.vue') },
      { path: '/buzon', name:"buzon", component: () => import('pages/Buzon.vue') }
    ]
  },
  {
    path: '/prepedido',
    component: () => import('layouts/LayoutPidePasa.vue'),
    children: [
      { path: '/', name:"prepedido", component: () => import('pages/PrePedido.vue') },
      { path: '/sucursales', name:"sucursales", component: () => import('pages/Sucursales.vue') },
      { path: '/completed/:ci', name:"completed-prepedido", component: () => import('pages/CompletedPrepedido.vue') },
    ]
  },
  {
    path: '/sesion',
    name:  'sesion',
    component: () => import('components/AuthUser.vue')
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
